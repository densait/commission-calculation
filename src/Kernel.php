<?php

namespace App;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection;

/**
 * Class Kernel
 * @package App
 */
class Kernel
{
    /**
     * @return DependencyInjection\ContainerBuilder
     * @throws Exception
     */
    public static function getDiContainer()
    {
        $container = new DependencyInjection\ContainerBuilder();
        (new DependencyInjection\Loader\YamlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../config')
        ))->load('services.yml');
        
        $container->compile();

        return $container;
    }
}
