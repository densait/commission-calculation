<?php

namespace App\Commands;

use App\Model\Commission;
use App\Model\Currency;
use App\Model\Transaction;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class CommissionCalculation
 * @package App\Commands
 */
class CommissionCalculation extends Command
{
    /**
     * @var Commission
     */
    protected Commission $commissionCalculator;

    /**
     * @var Currency
     */
    protected Currency $currency;

    /**
     * CommissionCalculation constructor.
     * @param Commission $commissionCalculator
     * @param Currency $currency
     * @param string|null $name
     */
    public function __construct(
        Commission $commissionCalculator,
        Currency $currency,
        ?string $name = null
    ) {
        parent::__construct($name);
        $this->commissionCalculator = $commissionCalculator;
        $this->currency = $currency;
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this->setName('commission:calculate')
            ->setDescription('Commission calculation')
            ->setHelp('Please provide CSV file with transactions.')
            ->addArgument('csv', InputArgument::REQUIRED, 'Pass the CSV file path.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $transactions = $this->getCsvData($input->getArgument('csv'));
        } catch (\Throwable $e) {
            $output->writeln('<error>Wrong CSV file provided</error>');
            return Command::FAILURE;
        }

        $this->commissionCalculator->calculate($transactions);
        foreach ($transactions as $transaction) {
            $output->writeln($this->currency->getFormattedAmount($transaction->commission, $transaction->currency));
        }

        return Command::SUCCESS;
    }

    /**
     * @param string $filePath
     * @return array
     * @throws Exception
     */
    protected function getCsvData(string $filePath): array
    {
        if (!file_exists($filePath)) {
            throw new Exception('CSV file does not exist.');
        }

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
        $data = $serializer->decode(file_get_contents($filePath), 'csv', ['no_headers' => true]);

        $output = [];
        foreach ($data as $row) {
            $output[] = new Transaction(...$row);
        }

        return $output;
    }
}
