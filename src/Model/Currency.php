<?php

namespace App\Model;

use App\Model\Currency\RatesProviderInterface;

/**
 * Class Currency
 * @package App\Model
 */
class Currency
{
    /**
     * Currency constructor.
     * @param array $noDecimalCurrencies
     * @param RatesProviderInterface $ratesProvider
     */
    public function __construct(
        private array $noDecimalCurrencies,
        private RatesProviderInterface $ratesProvider
    ) {}

    /**
     * @param float $amount
     * @param string $from
     * @param string $to
     * @return float
     */
    public function currencyConvert(float $amount, string $from, string $to): float
    {
        return $this->ratesProvider->convert($amount, $from, $to);
    }

    /**
     * @return array
     */
    public function getNoDecimalCurrencies(): array
    {
        return $this->noDecimalCurrencies;
    }

    /**
     * @param float $amount
     * @param string $currency
     * @return string
     */
    public function getFormattedAmount(float $amount, string $currency): string
    {
        if (in_array($currency, $this->getNoDecimalCurrencies())) {
            return $amount;
        }
        return sprintf("%01.2f", $amount);
    }

    /**
     * @param float $amount
     * @param string $currency
     * @return float
     */
    public function round(float $amount, string $currency): float
    {
        if (in_array($currency, $this->getNoDecimalCurrencies())) {
            return ceil($amount);
        } else {
            return ceil($amount * 100) / 100;
        }
    }
}
