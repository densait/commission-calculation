<?php

namespace App\Model\Commission;

use App\Model\Transaction;
use App\Model\Currency;

/**
 * Class PeriodAbstract
 * @package App\Model\Commission
 */
abstract class PeriodAbstract implements CalculatorInterface
{
    /**
     * @var array
     */
    private array $userStat = [];

    /**
     * PeriodAbstract constructor.
     * @param Currency $currency
     * @param string $baseCurrency
     * @param array $commissionRules
     */
    public function __construct(
        protected Currency $currency,
        protected string $baseCurrency,
        protected array $commissionRules = []
    ) {}

    /**
     * @param Transaction $transaction
     * @return float
     */
    public function calculate(Transaction $transaction): float
    {
        $userId = $transaction->userId;
        $periodKey = $this->getPeriodKeyByDate($transaction->date);
        if (empty($this->userStat[$userId][$periodKey])) {
            $this->userStat[$userId][$periodKey] = [
                'count' => 0,
                'sum' => 0,
            ];
        }

        $commission = null;
        $this->setBaseCurrencyAmount($transaction);

        foreach ($this->getCommissionRulesByUserType($transaction->userType) as $rule) {
            $commission = $rule->calculate($transaction, $this->userStat[$userId][$periodKey]);
            if (null !== $commission) {
                break;
            }
        }

        $this->userStat[$userId][$periodKey]['count']++;
        $this->userStat[$userId][$periodKey]['sum'] += $transaction->baseCurrencyAmount;

        return ($commission ?: 0);
    }

    /**
     * @param string $userType
     * @return array
     */
    protected function getCommissionRulesByUserType(string $userType): array
    {
        return $this->commissionRules[$userType] ?? [];
    }

    /**
     * @param Transaction $transaction
     * @return void
     */
    protected function setBaseCurrencyAmount(Transaction $transaction)
    {
        $transaction->baseCurrency = $this->baseCurrency;
        $transaction->baseCurrencyAmount = $this->currency->currencyConvert(
            $transaction->amount,
            $transaction->currency,
            $transaction->baseCurrency
        );
    }

    /**
     * @param string $date
     * @return string
     */
    abstract protected function getPeriodKeyByDate(string $date): string;
}
