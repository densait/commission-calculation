<?php

namespace App\Model\Commission\Rules;

use App\Model\Currency;
use App\Model\Transaction;

/**
 * Class FreeOfChangeSum
 * @package App\Model\Commission\Rules
 */
class FreeOfChangeSum implements RuleInterface
{
    /**
     * FreeOfChangeSum constructor.
     * @param float $freeOfChangeSum
     * @param float $percent
     * @param Currency $currency
     */
    public function __construct(
        private float $freeOfChangeSum,
        private float $percent,
        private Currency $currency
    ) {}

    /**
     * @param Transaction $transaction
     * @param array $userStat
     * @return float|null
     */
    public function calculate(Transaction $transaction, array $userStat): float|null
    {
        if ($userStat['sum'] > $this->freeOfChangeSum) {
            return $transaction->amount / 100 * $this->percent;
        } else if ($userStat['sum'] + $transaction->baseCurrencyAmount > $this->freeOfChangeSum) {
            $commissionAmount = $userStat['sum'] + $transaction->baseCurrencyAmount - $this->freeOfChangeSum;
            $commissionAmount = $this->currency->currencyConvert(
                $commissionAmount,
                $transaction->baseCurrency,
                $transaction->currency
            );
            return $commissionAmount / 100 * $this->percent;
        }
        return null;
    }
}
