<?php

namespace App\Model\Commission\Rules;

use App\Model\Transaction;

/**
 * Class Percent
 * @package App\Model\Commission\Rules
 */
class Percent implements RuleInterface
{
    /**
     * Percent constructor.
     * @param float $percent
     */
    public function __construct(
        private float $percent
    ) {}

    /**
     * @param Transaction $transaction
     * @param array $userStat
     * @return float
     */
    public function calculate(Transaction $transaction, array $userStat): float
    {
        return $transaction->amount / 100 * $this->percent;
    }
}
