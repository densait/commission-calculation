<?php

namespace App\Model\Commission\Rules;

use App\Model\Transaction;

/**
 * Interface RuleInterface
 * @package App\Model\Commission\Rules
 */
interface RuleInterface
{
    /**
     * @param Transaction $transaction
     * @param array $userStat
     * @return float|null
     */
    public function calculate(Transaction $transaction, array $userStat): float|null;
}
