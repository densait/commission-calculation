<?php

namespace App\Model\Commission\Rules;

use App\Model\Transaction;

/**
 * Class FreeOfChangeCount
 * @package App\Model\Commission\Rules
 */
class FreeOfChangeCount implements RuleInterface
{
    /**
     * FreeOfChangeCount constructor.
     * @param int $freeOfChangeCount
     * @param float $percent
     */
    public function __construct(
        private int $freeOfChangeCount,
        private float $percent
    ) {}

    /**
     * @param Transaction $transaction
     * @param array $userStat
     * @return float|null
     */
    public function calculate(Transaction $transaction, array $userStat): float|null
    {
        if ($userStat['count'] + 1 > $this->freeOfChangeCount) {
            return $transaction->amount / 100 * $this->percent;
        }
        return null;
    }
}
