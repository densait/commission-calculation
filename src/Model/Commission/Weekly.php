<?php

namespace App\Model\Commission;

/**
 * Class Weekly
 * @package App\Model\Commission
 */
class Weekly extends PeriodAbstract
{
    /**
     * @param string $date
     * @return string
     */
    protected function getPeriodKeyByDate(string $date): string
    {
        return date('Y_W', strtotime('Monday this week', strtotime($date)));
    }
}
