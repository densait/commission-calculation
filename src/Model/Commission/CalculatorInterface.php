<?php

namespace App\Model\Commission;

use App\Model\Transaction;

/**
 * Interface CalculatorInterface
 * @package App\Model\Commission
 */
interface CalculatorInterface
{
    /**
     * @param Transaction $transaction
     * @return float
     */
    public function calculate(Transaction $transaction): float;
}
