<?php

namespace App\Model\Currency;

/**
 * Class Sandbox
 * @package App\Model\Currency
 */
class Sandbox implements RatesProviderInterface
{
    /**
     * @param float $amount
     * @param string $from
     * @param string $to
     * @return float
     */
    public function convert(float $amount, string $from, string $to): float
    {
        #FOR TESTING PURPOSES ONLY!!!
        $testRates = [
            'EUR' => [
                'USD' => 1.1497,
                'JPY' => 129.53,
            ],
            'USD' => [
                'EUR' => (1 / 1.1497),
            ],
            'JPY' => [
                'EUR' => (1 / 129.53),
            ],
        ];

        if (!empty($testRates[$from][$to])) {
            return $amount * $testRates[$from][$to];
        }

        return $amount;
    }
}
