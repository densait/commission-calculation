<?php

namespace App\Model\Currency;

/**
 * Interface RatesProviderInterface
 * @package App\Model\Currency
 */
interface RatesProviderInterface
{
    /**
     * @param float $amount
     * @param string $from
     * @param string $to
     * @return float
     */
    public function convert(float $amount, string $from, string $to): float;
}
