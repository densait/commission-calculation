<?php

namespace App\Model\Currency;

use Exception;

/**
 * Class ExchangeRatesApi
 * @package App\Model\Currency
 */
class ExchangeRatesApi implements RatesProviderInterface
{
    /**
     * ExchangeRatesApi constructor.
     * @param string|null $exchangeratesapiToken
     */
    public function __construct(
        private ?string $exchangeratesapiToken = null
    ) {}

    /**
     * @param float $amount
     * @param string $from
     * @param string $to
     * @return float
     * @throws Exception
     */
    public function convert(float $amount, string $from, string $to): float
    {
        if (null === $this->exchangeratesapiToken) {
            throw new Exception('API Token is not provided');
        }

        //!!!!Just Copy-pase from https://exchangeratesapi.io/documentation/

        $endpoint = 'convert';
        // initialize CURL:
        $ch = curl_init(
            'https://api.exchangeratesapi.io/v1/'
            . $endpoint
            . '?access_key=' . $this->exchangeratesapiToken
            . '&from=' . $from
            . '&to=' . $to
            . '&amount=' . $amount . ''
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // get the JSON data:
        $json = curl_exec($ch);
        curl_close($ch);

        // Decode JSON response:
        $conversionResult = json_decode($json, true);

        // access the conversion result
        return $conversionResult['result'];
    }
}
