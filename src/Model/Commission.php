<?php

namespace App\Model;

use App\Model\Commission\CalculatorInterface;
use Exception;

/**
 * Class Commission
 * @package App\Model
 */
class Commission
{
    /**
     * Commission constructor.
     * @param Currency $currency
     * @param array $operationTypes
     */
    public function __construct(
        private Currency $currency,
        private array $operationTypes = []
    ) {}

    /**
     * @param array $transactions
     * @return void
     * @throws Exception
     */
    public function calculate(array $transactions)
    {
        foreach ($transactions as $transaction) {
            $commission = $this->getOperationTypeCalculator($transaction->operationType)->calculate($transaction);
            $transaction->commission = $this->currency->round($commission, $transaction->currency);
        }
    }

    /**
     * @param string $operationType
     * @return CalculatorInterface
     * @throws Exception
     */
    protected function getOperationTypeCalculator(string $operationType): CalculatorInterface
    {
        foreach ($this->operationTypes as $type => $calculator) {
            if ($operationType === $type) {
                return $calculator;
            }
        }
        throw new Exception('Wrong operation type');
    }
}
