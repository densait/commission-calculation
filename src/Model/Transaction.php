<?php

namespace App\Model;

/**
 * Class Transaction
 * @package App\Model
 */
class Transaction
{
    /**
     * @var string
     */
    public string $baseCurrency;

    /**
     * @var float
     */
    public float $baseCurrencyAmount;

    /**
     * Transaction constructor.
     * @param string $date
     * @param int $userId
     * @param string $userType
     * @param string $operationType
     * @param float $amount
     * @param string $currency
     * @param float $commission
     */
    public function __construct(
        public string $date,
        public int $userId,
        public string $userType,
        public string $operationType,
        public float $amount,
        public string $currency,
        public float $commission = 0
    ) {}
}
