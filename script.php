#!/usr/bin/env php
<?php

require_once __DIR__ . '/vendor/autoload.php';

$container = \App\Kernel::getDiContainer();
$app = $container->get(Symfony\Component\Console\Application::class);
$commissionCalculation = $container->get(App\Commands\CommissionCalculation::class);

$app->add($commissionCalculation);
$app->setDefaultCommand($commissionCalculation->getName());

$app->run();
