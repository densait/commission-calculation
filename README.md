# Commission calculation
by Denis Yurevich

---
## Running the code
- Use the following CLI command to run the task: `php script.php commission:calculate input.csv`
- Use the following CLI command to run the tests: `vendor/bin/phpunit tests`
- FYI: Tested on PHP 8

## Configurations
All the configurations are here: `config/services.yml`
> It's possible to configure the followings without changing the code:
>> 1. Default currency (EUR);
>> 2. No decimal currencies (JPY);
>> 3. Ability to add new account types;
>> 4. Ability to add new operation types;
>> 5. Ability to change the commission fees for rules;
>> 6. Ability to change the free of change amount;
>> 7. Ability to change the free of change operations count;
>> 8. Ability to set up different rules for different operation types and different account types;
>> 9. Ability to develop and use new rules
