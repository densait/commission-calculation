<?php

namespace App\Tests\Model;

use App\Kernel;
use App\Model\Commission;
use App\Model\Transaction;
use Exception;
use PHPUnit\Framework\TestCase;

/**
 * Class CommissionTest
 * @package App\Tests\Model
 */
class CommissionTest extends TestCase
{
    /**
     * @var Commission
     */
    private Commission $commissionCalculator;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        $container = Kernel::getDiContainer();
        $this->commissionCalculator = $container->get(Commission::class);
    }

    /**
     * @dataProvider dataProvider
     * @throws Exception
     */
    public function testCommissions($transactions, $commissions): void
    {
        $this->commissionCalculator->calculate($transactions);
        foreach ($transactions as $id => $transaction) {
            $this->assertEquals($transaction->commission, $commissions[$id]);
        }
    }

    /**
     * @return array[]
     */
    public function dataProvider()
    {
        return [
            [
                [
                    new Transaction('2014-12-31', 4, 'private', 'withdraw', 1000, 'EUR'),
                    new Transaction('2014-12-31', 4, 'private', 'withdraw', 1000, 'EUR'),
                    new Transaction('2014-12-10', 4, 'private', 'withdraw', 1500, 'EUR'),
                    new Transaction('2014-12-10', 4, 'private', 'withdraw', 100, 'EUR'),
                    new Transaction('2014-12-10', 1, 'business', 'deposit', 10000, 'EUR'),
                ],
                [
                    0,
                    3,
                    1.5,
                    0.3,
                    3
                ]
            ]
        ];
    }
}
