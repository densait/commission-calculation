<?php

namespace App\Tests\Model\Commission\Rules;

use App\Model\Commission\Rules\Percent;
use App\Model\Transaction;
use PHPUnit\Framework\TestCase;

/**
 * Class PercentTest
 * @package App\Tests\Model\Commission\Rules
 */
class PercentTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testCalculate($transaction, $rule, $expectedCommission): void
    {
        $commission = $rule->calculate($transaction, []);
        $this->assertEquals($commission, $expectedCommission);
    }

    /**
     * @return array[]
     */
    public function dataProvider()
    {
        return [
            [
                new Transaction('2014-12-31', 4, 'private', 'withdraw', 1000, 'EUR'),
                new Percent(10),
                100
            ],
            [
                new Transaction('2014-12-31', 4, 'private', 'withdraw', 1000, 'EUR'),
                new Percent(15),
                150
            ]
        ];
    }
}
