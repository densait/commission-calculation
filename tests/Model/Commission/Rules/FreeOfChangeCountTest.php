<?php

namespace App\Tests\Model\Commission\Rules;

use App\Model\Commission\Rules\FreeOfChangeCount;
use App\Model\Transaction;
use PHPUnit\Framework\TestCase;

/**
 * Class FreeOfChangeCountTest
 * @package App\Tests\Model\Commission\Rules
 */
class FreeOfChangeCountTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testCalculate($transaction, $rule, $userStat, $expectedCommission): void
    {
        $commission = $rule->calculate($transaction, $userStat);
        $this->assertEquals($commission, $expectedCommission);
    }

    /**
     * @return array[]
     */
    public function dataProvider()
    {
        return [
            [
                new Transaction('2014-12-31', 4, 'private', 'withdraw', 1000, 'EUR'),
                new FreeOfChangeCount(3, 10),
                ['count' => 2],
                null
            ],
            [
                new Transaction('2014-12-31', 4, 'private', 'withdraw', 1000, 'EUR'),
                new FreeOfChangeCount(3, 10),
                ['count' => 3],
                100
            ]
        ];
    }
}
